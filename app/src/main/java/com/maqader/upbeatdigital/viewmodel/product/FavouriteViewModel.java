package com.maqader.upbeatdigital.viewmodel.product;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.maqader.upbeatdigital.repository.product.ProductRepository;
import com.maqader.upbeatdigital.utils.AbsentLiveData;
import com.maqader.upbeatdigital.viewmodel.common.PSViewModel;
import com.maqader.upbeatdigital.viewobject.common.Resource;

import javax.inject.Inject;

public class FavouriteViewModel extends PSViewModel {
    private final LiveData<Resource<Boolean>> sendFavouritePostData;
    private MutableLiveData<FavouriteViewModel.TmpDataHolder> sendFavouriteDataPostObj = new MutableLiveData<>();

    @Inject
    public FavouriteViewModel(ProductRepository productRepository) {
        sendFavouritePostData = Transformations.switchMap(sendFavouriteDataPostObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }
            return productRepository.uploadFavouritePostToServer(obj.productId, obj.userId, obj.shopId);
        });
    }

    public void setFavouritePostDataObj(String product_id, String userId, String shopId) {

        if (!isLoading) {
            FavouriteViewModel.TmpDataHolder tmpDataHolder = new FavouriteViewModel.TmpDataHolder();
            tmpDataHolder.productId = product_id;
            tmpDataHolder.userId = userId;
            tmpDataHolder.shopId = shopId;

            sendFavouriteDataPostObj.setValue(tmpDataHolder);
            // start loading
            setLoadingState(true);
        }
    }

    public LiveData<Resource<Boolean>> getFavouritePostData() {
        return sendFavouritePostData;
    }

    class TmpDataHolder {
        public String productId = "";
        public String userId = "";
        public String shopId = "";
    }
}
