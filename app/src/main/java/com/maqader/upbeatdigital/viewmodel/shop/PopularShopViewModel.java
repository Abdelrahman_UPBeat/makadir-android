package com.maqader.upbeatdigital.viewmodel.shop;

import com.maqader.upbeatdigital.repository.shop.ShopRepository;
import com.maqader.upbeatdigital.utils.AbsentLiveData;
import com.maqader.upbeatdigital.viewmodel.common.PSViewModel;
import com.maqader.upbeatdigital.viewobject.Shop;
import com.maqader.upbeatdigital.viewobject.common.Resource;
import com.maqader.upbeatdigital.viewobject.holder.ShopParameterHolder;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class PopularShopViewModel extends PSViewModel {


    //region Variables
    private final LiveData<Resource<List<Shop>>> popularShopListData;
    private MutableLiveData<TmpDataHolder> popularShopListObj = new MutableLiveData<>();

    public ShopParameterHolder shopParameterHolder = new ShopParameterHolder().getPopularParameterHolder();
    //endregion


    //region Constructor
    @Inject
    PopularShopViewModel(ShopRepository repository) {

        popularShopListData = Transformations.switchMap(popularShopListObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            return repository.getShopList(obj.apiKey, obj.limit, obj.offset, obj.parameterHolder);

        });

    }

    //endregion


    //region Popular Shop List

    public void setPopularShopListObj(String apiKey, String limit, String offset, ShopParameterHolder parameterHolder) {
        TmpDataHolder tmpDataHolder = new TmpDataHolder(apiKey, limit, offset, parameterHolder);

        this.popularShopListObj.setValue(tmpDataHolder);
    }

    public LiveData<Resource<List<Shop>>> getPopularShopListData() {
        return popularShopListData;
    }

    //endregion


    //region Holders

    class TmpDataHolder {

        private String apiKey, limit, offset;
        private ShopParameterHolder parameterHolder;

        public TmpDataHolder(String apiKey, String limit, String offset, ShopParameterHolder parameterHolder) {
            this.apiKey = apiKey;
            this.limit = limit;
            this.offset = offset;
            this.parameterHolder = parameterHolder;
        }
    }

    //endregion
}
