package com.maqader.upbeatdigital.viewmodel.coupondiscount;

import com.maqader.upbeatdigital.repository.coupondiscount.CouponDiscountRepository;
import com.maqader.upbeatdigital.utils.AbsentLiveData;
import com.maqader.upbeatdigital.viewmodel.common.PSViewModel;
import com.maqader.upbeatdigital.viewobject.CouponDiscount;
import com.maqader.upbeatdigital.viewobject.common.Resource;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class CouponDiscountViewModel extends PSViewModel {

    private final LiveData<Resource<CouponDiscount>> couponDiscountData;
    private final MutableLiveData<CouponDiscountViewModel.TmpDataHolder> couponDiscountObj = new MutableLiveData<>();

    @Inject
    CouponDiscountViewModel(CouponDiscountRepository repository) {

        couponDiscountData = Transformations.switchMap(couponDiscountObj, obj -> {

            if (obj == null) {
                return AbsentLiveData.create();
            }

            return repository.getCouponDiscount(obj.code, obj.shopId);
        });
    }

    public void setCouponDiscountObj(String code, String shopId) {
        TmpDataHolder tmpDataHolder = new TmpDataHolder();
        tmpDataHolder.code = code;
        tmpDataHolder.shopId = shopId;

        couponDiscountObj.setValue(tmpDataHolder);
    }

    public LiveData<Resource<CouponDiscount>> getCouponDiscountData() {
        return couponDiscountData;
    }


    class TmpDataHolder {

        public String code = "";
        public String shopId = "";
    }

}
