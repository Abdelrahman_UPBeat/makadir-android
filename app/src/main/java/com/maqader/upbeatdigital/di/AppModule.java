package com.maqader.upbeatdigital.di;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maqader.upbeatdigital.Config;
import com.maqader.upbeatdigital.api.PSApiService;
import com.maqader.upbeatdigital.db.AboutUsDao;
import com.maqader.upbeatdigital.db.BasketDao;
import com.maqader.upbeatdigital.db.BlogDao;
import com.maqader.upbeatdigital.db.CategoryDao;
import com.maqader.upbeatdigital.db.CategoryMapDao;
import com.maqader.upbeatdigital.db.CityDao;
import com.maqader.upbeatdigital.db.CommentDao;
import com.maqader.upbeatdigital.db.CommentDetailDao;
import com.maqader.upbeatdigital.db.CountryDao;
import com.maqader.upbeatdigital.db.DeletedObjectDao;
import com.maqader.upbeatdigital.db.HistoryDao;
import com.maqader.upbeatdigital.db.ImageDao;
import com.maqader.upbeatdigital.db.NotificationDao;
import com.maqader.upbeatdigital.db.PSAppInfoDao;
import com.maqader.upbeatdigital.db.PSAppVersionDao;
import com.maqader.upbeatdigital.db.PSCoreDb;
import com.maqader.upbeatdigital.db.ProductAttributeDetailDao;
import com.maqader.upbeatdigital.db.ProductAttributeHeaderDao;
import com.maqader.upbeatdigital.db.ProductCollectionDao;
import com.maqader.upbeatdigital.db.ProductColorDao;
import com.maqader.upbeatdigital.db.ProductDao;
import com.maqader.upbeatdigital.db.ProductMapDao;
import com.maqader.upbeatdigital.db.ProductSpecsDao;
import com.maqader.upbeatdigital.db.RatingDao;
import com.maqader.upbeatdigital.db.ShippingMethodDao;
import com.maqader.upbeatdigital.db.ShopDao;
import com.maqader.upbeatdigital.db.ShopListByTagIdDao;
import com.maqader.upbeatdigital.db.ShopTagDao;
import com.maqader.upbeatdigital.db.SubCategoryDao;
import com.maqader.upbeatdigital.db.TransactionDao;
import com.maqader.upbeatdigital.db.TransactionOrderDao;
import com.maqader.upbeatdigital.db.UserDao;
import com.maqader.upbeatdigital.utils.AppLanguage;
import com.maqader.upbeatdigital.utils.Connectivity;
import com.maqader.upbeatdigital.utils.Constants;
import com.maqader.upbeatdigital.utils.LiveDataCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.room.Room;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module(includes = ViewModelModule.class)
class AppModule {


    @Inject
    SharedPreferences sharedPreferences;

    @Singleton
    @Provides
    PSApiService providePSApiService() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

//        OkHttpClient.Builder okHttpClient; = new OkHttpClient.Builder()
//                .writeTimeout(1, TimeUnit.MINUTES)
//                .readTimeout(1, TimeUnit.MINUTES)
//                .build();
        OkHttpClient.Builder okHttpClient;

        okHttpClient = new OkHttpClient.Builder();
        AppLanguage appLanguage= provideCurrentLanguage(sharedPreferences);
        Log.d("Languageeeee",appLanguage.getLanguage(true));

        okHttpClient.addInterceptor(chain -> {
            Request original = chain.request();
            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder()
                    .addHeader("Accept-Language", appLanguage.getLanguage(true))
                    .addHeader("Accept", "application/json");
            Request request = requestBuilder.build();
            return chain.proceed(request);
        });




        return new Retrofit.Builder()
                .baseUrl(Config.APP_API_URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(PSApiService.class);

    }

    @Singleton
    @Provides
    PSCoreDb provideDb(Application app) {
        return Room.databaseBuilder(app, PSCoreDb.class, "PSApp.db")
                //.addMigrations(MIGRATION_1_2)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    Connectivity provideConnectivity(Application app) {
        return new Connectivity(app);
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences(Application app) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
        return PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
    }

    @Singleton
    @Provides
    UserDao provideUserDao(PSCoreDb db) {
        return db.userDao();
    }

    @Singleton
    @Provides
    AppLanguage provideCurrentLanguage(SharedPreferences sharedPreferences) {

        return new AppLanguage(sharedPreferences);
    }




    @Singleton
    @Provides
    AboutUsDao provideAboutUsDao(PSCoreDb db) {
        return db.aboutUsDao();
    }

    @Singleton
    @Provides
    ImageDao provideImageDao(PSCoreDb db) {
        return db.imageDao();
    }

    @Singleton
    @Provides
    CountryDao provideCountryDao(PSCoreDb db) {
        return db.countryDao();
    }

    @Singleton
    @Provides
    CityDao provideCityDao(PSCoreDb db) {
        return db.cityDao();
    }

    @Singleton
    @Provides
    ProductDao provideProductDao(PSCoreDb db) {
        return db.productDao();
    }

    @Singleton
    @Provides
    ProductColorDao provideProductColorDao(PSCoreDb db) {
        return db.productColorDao();
    }

    @Singleton
    @Provides
    ProductSpecsDao provideProductSpecsDao(PSCoreDb db) {
        return db.productSpecsDao();
    }

    @Singleton
    @Provides
    ProductAttributeHeaderDao provideProductAttributeHeaderDao(PSCoreDb db) {
        return db.productAttributeHeaderDao();
    }

    @Singleton
    @Provides
    ProductAttributeDetailDao provideProductAttributeDetailDao(PSCoreDb db) {
        return db.productAttributeDetailDao();
    }

    @Singleton
    @Provides
    BasketDao provideBasketDao(PSCoreDb db) {
        return db.basketDao();
    }

    @Singleton
    @Provides
    HistoryDao provideHistoryDao(PSCoreDb db) {
        return db.historyDao();
    }

    @Singleton
    @Provides
    CategoryDao provideCategoryDao(PSCoreDb db) {
        return db.categoryDao();
    }

    @Singleton
    @Provides
    RatingDao provideRatingDao(PSCoreDb db) {
        return db.ratingDao();
    }

    @Singleton
    @Provides
    SubCategoryDao provideSubCategoryDao(PSCoreDb db) {
        return db.subCategoryDao();
    }

    @Singleton
    @Provides
    CommentDao provideCommentDao(PSCoreDb db) {
        return db.commentDao();
    }

    @Singleton
    @Provides
    CommentDetailDao provideCommentDetailDao(PSCoreDb db) {
        return db.commentDetailDao();
    }

    @Singleton
    @Provides
    NotificationDao provideNotificationDao(PSCoreDb db) {
        return db.notificationDao();
    }

    @Singleton
    @Provides
    ProductCollectionDao provideProductCollectionDao(PSCoreDb db) {
        return db.productCollectionDao();
    }

    @Singleton
    @Provides
    TransactionDao provideTransactionDao(PSCoreDb db) {
        return db.transactionDao();
    }

    @Singleton
    @Provides
    TransactionOrderDao provideTransactionOrderDao(PSCoreDb db) {
        return db.transactionOrderDao();
    }

//    @Singleton
//    @Provides
//    TrendingCategoryDao provideTrendingCategoryDao(PSCoreDb db){return db.trendingCategoryDao();}

    @Singleton
    @Provides
    ShopDao provideShopDao(PSCoreDb db) {
        return db.shopDao();
    }

    @Singleton
    @Provides
    ShopTagDao provideShopCategoryDao(PSCoreDb db) {
        return db.shopCategoryDao();
    }

    @Singleton
    @Provides
    BlogDao provideNewsFeedDao(PSCoreDb db) {
        return db.blogDao();
    }

    @Singleton
    @Provides
    ShippingMethodDao provideShippingMethodDao(PSCoreDb db) {
        return db.shippingMethodDao();
    }

    @Singleton
    @Provides
    ShopListByTagIdDao provideShopCategoryByIdDao(PSCoreDb db) {
        return db.shopListByTagIdDao();
    }

    @Singleton
    @Provides
    ProductMapDao provideProductMapDao(PSCoreDb db) {
        return db.productMapDao();
    }

    @Singleton
    @Provides
    CategoryMapDao provideCategoryMapDao(PSCoreDb db) {
        return db.categoryMapDao();
    }

    @Singleton
    @Provides
    PSAppInfoDao providePSAppInfoDao(PSCoreDb db) {
        return db.psAppInfoDao();
    }

    @Singleton
    @Provides
    PSAppVersionDao providePSAppVersionDao(PSCoreDb db) {
        return db.psAppVersionDao();
    }

    @Singleton
    @Provides
    DeletedObjectDao provideDeletedObjectDao(PSCoreDb db) {
        return db.deletedObjectDao();
    }
}
