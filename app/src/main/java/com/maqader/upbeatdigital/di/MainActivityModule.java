package com.maqader.upbeatdigital.di;


import com.maqader.upbeatdigital.MainActivity;
import com.maqader.upbeatdigital.ui.apploading.AppLoadingActivity;
import com.maqader.upbeatdigital.ui.apploading.AppLoadingFragment;
import com.maqader.upbeatdigital.ui.basket.BasketListActivity;
import com.maqader.upbeatdigital.ui.basket.BasketListFragment;
import com.maqader.upbeatdigital.ui.blog.detail.BlogDetailActivity;
import com.maqader.upbeatdigital.ui.blog.detail.BlogDetailFragment;
import com.maqader.upbeatdigital.ui.blog.list.BlogListActivity;
import com.maqader.upbeatdigital.ui.blog.list.BlogListFragment;
import com.maqader.upbeatdigital.ui.blog.listbyshopid.BlogListByShopIdActivity;
import com.maqader.upbeatdigital.ui.blog.listbyshopid.BlogListByShopIdFragment;
import com.maqader.upbeatdigital.ui.category.CategoryListActivity;
import com.maqader.upbeatdigital.ui.category.CategoryListFragment;
import com.maqader.upbeatdigital.ui.category.TrendingCategoryFragment;
import com.maqader.upbeatdigital.ui.checkout.CheckoutActivity;
import com.maqader.upbeatdigital.ui.checkout.CheckoutFragment1;
import com.maqader.upbeatdigital.ui.checkout.CheckoutFragment2;
import com.maqader.upbeatdigital.ui.checkout.CheckoutFragment3;
import com.maqader.upbeatdigital.ui.checkout.CheckoutStatusFragment;
import com.maqader.upbeatdigital.ui.privacyandpolicy.PrivacyAndPolicyActivity;
import com.maqader.upbeatdigital.ui.privacyandpolicy.PrivacyAndPolicyFragment;
import com.maqader.upbeatdigital.ui.product.search.SearchCityListFragment;
import com.maqader.upbeatdigital.ui.product.search.SearchCountryListFragment;
import com.maqader.upbeatdigital.ui.stripe.StripeActivity;
import com.maqader.upbeatdigital.ui.collection.CollectionActivity;
import com.maqader.upbeatdigital.ui.collection.CollectionFragment;
import com.maqader.upbeatdigital.ui.collection.productCollectionHeader.ProductCollectionHeaderListActivity;
import com.maqader.upbeatdigital.ui.collection.productCollectionHeader.ProductCollectionHeaderListFragment;
import com.maqader.upbeatdigital.ui.comment.detail.CommentDetailActivity;
import com.maqader.upbeatdigital.ui.comment.detail.CommentDetailFragment;
import com.maqader.upbeatdigital.ui.comment.list.CommentListActivity;
import com.maqader.upbeatdigital.ui.comment.list.CommentListFragment;
import com.maqader.upbeatdigital.ui.contactus.ContactUsFragment;
import com.maqader.upbeatdigital.ui.dashboard.DashBoardShopListFragment;
import com.maqader.upbeatdigital.ui.forceupdate.ForceUpdateActivity;
import com.maqader.upbeatdigital.ui.forceupdate.ForceUpdateFragment;
import com.maqader.upbeatdigital.ui.gallery.GalleryActivity;
import com.maqader.upbeatdigital.ui.gallery.GalleryFragment;
import com.maqader.upbeatdigital.ui.gallery.detail.GalleryDetailActivity;
import com.maqader.upbeatdigital.ui.gallery.detail.GalleryDetailFragment;
import com.maqader.upbeatdigital.ui.language.LanguageFragment;
import com.maqader.upbeatdigital.ui.notification.detail.NotificationActivity;
import com.maqader.upbeatdigital.ui.notification.detail.NotificationFragment;
import com.maqader.upbeatdigital.ui.notification.list.NotificationListActivity;
import com.maqader.upbeatdigital.ui.notification.list.NotificationListFragment;
import com.maqader.upbeatdigital.ui.notification.setting.NotificationSettingFragment;
import com.maqader.upbeatdigital.ui.product.detail.ProductActivity;
import com.maqader.upbeatdigital.ui.product.detail.ProductDetailFragment;
import com.maqader.upbeatdigital.ui.product.favourite.FavouriteListActivity;
import com.maqader.upbeatdigital.ui.product.favourite.FavouriteListFragment;
import com.maqader.upbeatdigital.ui.product.filtering.CategoryFilterFragment;
import com.maqader.upbeatdigital.ui.product.filtering.FilterFragment;
import com.maqader.upbeatdigital.ui.product.filtering.FilteringActivity;
import com.maqader.upbeatdigital.ui.product.history.HistoryFragment;
import com.maqader.upbeatdigital.ui.product.history.UserHistoryListActivity;
import com.maqader.upbeatdigital.ui.product.list.ProductListActivity;
import com.maqader.upbeatdigital.ui.product.list.ProductListFragment;
import com.maqader.upbeatdigital.ui.product.productbycatId.ProductListByCatIdActivity;
import com.maqader.upbeatdigital.ui.product.productbycatId.ProductListByCatIdFragment;
import com.maqader.upbeatdigital.ui.product.search.SearchByCategoryActivity;
import com.maqader.upbeatdigital.ui.product.search.SearchCategoryFragment;
import com.maqader.upbeatdigital.ui.product.search.SearchFragment;
import com.maqader.upbeatdigital.ui.product.search.SearchSubCategoryFragment;
import com.maqader.upbeatdigital.ui.rating.RatingListActivity;
import com.maqader.upbeatdigital.ui.rating.RatingListFragment;
import com.maqader.upbeatdigital.ui.setting.AppInfoActivity;
import com.maqader.upbeatdigital.ui.setting.AppInfoFragment;
import com.maqader.upbeatdigital.ui.setting.TermsAndConditionsActivity;
import com.maqader.upbeatdigital.ui.setting.NotificationSettingActivity;
import com.maqader.upbeatdigital.ui.setting.SettingActivity;
import com.maqader.upbeatdigital.ui.setting.SettingFragment;
import com.maqader.upbeatdigital.ui.shop.detail.ShopFragment;
import com.maqader.upbeatdigital.ui.shop.list.ShopListActivity;
import com.maqader.upbeatdigital.ui.shop.list.ShopListFragment;
import com.maqader.upbeatdigital.ui.shop.listbytagid.ShopListByTagIdActivity;
import com.maqader.upbeatdigital.ui.shop.listbytagid.ShopListByTagIdFragment;
import com.maqader.upbeatdigital.ui.shop.menu.ShopMenuFragment;
import com.maqader.upbeatdigital.ui.shop.selectedshop.SelectedShopActivity;
import com.maqader.upbeatdigital.ui.shop.selectedshop.SelectedShopFragment;
import com.maqader.upbeatdigital.ui.shop.tag.ShopTagListActivity;
import com.maqader.upbeatdigital.ui.shop.tag.ShopTagListFragment;
import com.maqader.upbeatdigital.ui.stripe.StripeFragment;
import com.maqader.upbeatdigital.ui.terms.TermsAndConditionsFragment;
import com.maqader.upbeatdigital.ui.transaction.detail.TransactionActivity;
import com.maqader.upbeatdigital.ui.transaction.detail.TransactionFragment;
import com.maqader.upbeatdigital.ui.transaction.list.TransactionListActivity;
import com.maqader.upbeatdigital.ui.transaction.list.TransactionListFragment;
import com.maqader.upbeatdigital.ui.user.PasswordChangeActivity;
import com.maqader.upbeatdigital.ui.user.PasswordChangeFragment;
import com.maqader.upbeatdigital.ui.user.ProfileEditActivity;
import com.maqader.upbeatdigital.ui.user.ProfileEditFragment;
import com.maqader.upbeatdigital.ui.user.ProfileFragment;
import com.maqader.upbeatdigital.ui.user.UserForgotPasswordActivity;
import com.maqader.upbeatdigital.ui.user.UserForgotPasswordFragment;
import com.maqader.upbeatdigital.ui.user.UserLoginActivity;
import com.maqader.upbeatdigital.ui.user.UserLoginFragment;
import com.maqader.upbeatdigital.ui.user.UserRegisterActivity;
import com.maqader.upbeatdigital.ui.user.UserRegisterFragment;
import com.maqader.upbeatdigital.ui.user.verifyemail.VerifyEmailActivity;
import com.maqader.upbeatdigital.ui.user.verifyemail.VerifyEmailFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Panacea-Soft on 11/15/17.
 * Contact Email : teamps.is.cool@gmail.com
 */


@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = TransactionModule.class)
    abstract TransactionListActivity contributeTransactionActivity();

    @ContributesAndroidInjector(modules = FavouriteListModule.class)
    abstract FavouriteListActivity contributeFavouriteListActivity();

    @ContributesAndroidInjector(modules = UserHistoryModule.class)
    abstract UserHistoryListActivity contributeUserHistoryListActivity();

    @ContributesAndroidInjector(modules = UserRegisterModule.class)
    abstract UserRegisterActivity contributeUserRegisterActivity();

    @ContributesAndroidInjector(modules = UserForgotPasswordModule.class)
    abstract UserForgotPasswordActivity contributeUserForgotPasswordActivity();

    @ContributesAndroidInjector(modules = UserLoginModule.class)
    abstract UserLoginActivity contributeUserLoginActivity();

    @ContributesAndroidInjector(modules = PasswordChangeModule.class)
    abstract PasswordChangeActivity contributePasswordChangeActivity();

    @ContributesAndroidInjector(modules = ProductListByCatIdModule.class)
    abstract ProductListByCatIdActivity productListByCatIdActivity();

    @ContributesAndroidInjector(modules = FilteringModule.class)
    abstract FilteringActivity filteringActivity();

    @ContributesAndroidInjector(modules = CommentDetailModule.class)
    abstract CommentDetailActivity commentDetailActivity();

    @ContributesAndroidInjector(modules = DiscountDetailModule.class)
    abstract ProductActivity discountDetailActivity();

    @ContributesAndroidInjector(modules = NotificationModule.class)
    abstract NotificationListActivity notificationActivity();

    @ContributesAndroidInjector(modules = HomeFilteringActivityModule.class)
    abstract ProductListActivity contributehomeFilteringActivity();

    @ContributesAndroidInjector(modules = NotificationDetailModule.class)
    abstract NotificationActivity notificationDetailActivity();

    @ContributesAndroidInjector(modules = TransactionDetailModule.class)
    abstract TransactionActivity transactionDetailActivity();

    @ContributesAndroidInjector(modules = CheckoutActivityModule.class)
    abstract CheckoutActivity checkoutActivity();

    @ContributesAndroidInjector(modules = CommentListActivityModule.class)
    abstract CommentListActivity commentListActivity();

    @ContributesAndroidInjector(modules = BasketlistActivityModule.class)
    abstract BasketListActivity basketListActivity();

    @ContributesAndroidInjector(modules = GalleryDetailActivityModule.class)
    abstract GalleryDetailActivity galleryDetailActivity();

    @ContributesAndroidInjector(modules = GalleryActivityModule.class)
    abstract GalleryActivity galleryActivity();

    @ContributesAndroidInjector(modules = SearchByCategoryActivityModule.class)
    abstract SearchByCategoryActivity searchByCategoryActivity();

    @ContributesAndroidInjector(modules = TermsAndConditionsModule.class)
    abstract com.maqader.upbeatdigital.ui.terms.TermsAndConditionsActivity termsAndConditionsActivity();

    @ContributesAndroidInjector(modules = EditSettingModule.class)
    abstract SettingActivity editSettingActivity();

    @ContributesAndroidInjector(modules = LanguageChangeModule.class)
    abstract NotificationSettingActivity languageChangeActivity();

    @ContributesAndroidInjector(modules = ProfileEditModule.class)
    abstract ProfileEditActivity contributeProfileEditActivity();

    @ContributesAndroidInjector(modules = TermsAndConditionsModule.class)
    abstract TermsAndConditionsActivity ConditionsAndTermsActivity();

    @ContributesAndroidInjector(modules = AppInfoModule.class)
    abstract AppInfoActivity AppInfoActivity();

    @ContributesAndroidInjector(modules = ProductCollectionModule.class)
    abstract ProductCollectionHeaderListActivity productCollectionHeaderListActivity();

    @ContributesAndroidInjector(modules = CategoryListActivityAppInfoModule.class)
    abstract CategoryListActivity categoryListActivity();

    @ContributesAndroidInjector(modules = RatingListActivityModule.class)
    abstract RatingListActivity ratingListActivity();

    @ContributesAndroidInjector(modules = ShopListModule.class)
    abstract SelectedShopActivity selectedShopActivity();

    @ContributesAndroidInjector(modules = CollectionModule.class)
    abstract CollectionActivity collectionActivity();

    @ContributesAndroidInjector(modules = StripeModule.class)
    abstract StripeActivity stripeActivity();

    @ContributesAndroidInjector(modules = SelectedShopListModule.class)
    abstract ShopListActivity selectedShopListActivity();

    @ContributesAndroidInjector(modules = SelectedShopListBlogModule.class)
    abstract BlogListActivity selectedShopListBlogActivity();

    @ContributesAndroidInjector(modules = BlogDetailModule.class)
    abstract BlogDetailActivity blogDetailActivity();

    @ContributesAndroidInjector(modules = ShopCategoryDetailModule.class)
    abstract ShopListByTagIdActivity shopCategoryDetailActivity();

    @ContributesAndroidInjector(modules = ShopCategoryViewAllModule.class)
    abstract ShopTagListActivity shopCategoryViewAllActivity();

    @ContributesAndroidInjector(modules = forceUpdateModule.class)
    abstract ForceUpdateActivity forceUpdateActivity();

    @ContributesAndroidInjector(modules = blogListByShopIdActivityModule.class)
    abstract BlogListByShopIdActivity forceBlogListByShopIdActivity();

    @ContributesAndroidInjector(modules = appLoadingActivityModule.class)
    abstract AppLoadingActivity forceAppLoadingActivity();

    @ContributesAndroidInjector(modules = PrivacyAndPolicyActivityModule.class)
    abstract PrivacyAndPolicyActivity privacyAndPolicyActivity();

    @ContributesAndroidInjector(modules = VerifyEmailModule.class)
    abstract VerifyEmailActivity contributeVerifyEmailActivity();
}

@Module
abstract class CheckoutActivityModule {

    @ContributesAndroidInjector
    abstract CheckoutFragment1 checkoutFragment1();

    @ContributesAndroidInjector
    abstract LanguageFragment languageFragment();

    @ContributesAndroidInjector
    abstract CheckoutFragment2 checkoutFragment2();

    @ContributesAndroidInjector
    abstract CheckoutFragment3 checkoutFragment3();

    @ContributesAndroidInjector
    abstract CheckoutStatusFragment checkoutStatusFragment();
}

@Module
abstract class MainModule {

    @ContributesAndroidInjector
    abstract ContactUsFragment contributeContactUsFragment();

    @ContributesAndroidInjector
    abstract UserLoginFragment contributeUserLoginFragment();

    @ContributesAndroidInjector
    abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();

    @ContributesAndroidInjector
    abstract UserRegisterFragment contributeUserRegisterFragment();

    @ContributesAndroidInjector
    abstract NotificationSettingFragment contributeNotificationSettingFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment contributeProfileFragment();

    @ContributesAndroidInjector
    abstract LanguageFragment contributeLanguageFragment();

    @ContributesAndroidInjector
    abstract FavouriteListFragment contributeFavouriteListFragment();

    @ContributesAndroidInjector
    abstract TransactionListFragment contributTransactionListFragment();

    @ContributesAndroidInjector
    abstract SettingFragment contributEditSettingFragment();

    @ContributesAndroidInjector
    abstract HistoryFragment historyFragment();

    @ContributesAndroidInjector
    abstract NotificationListFragment contributeNotificationFragment();


    @ContributesAndroidInjector
    abstract AppInfoFragment contributeAppInfoFragment();

    @ContributesAndroidInjector
    abstract DashBoardShopListFragment contributeShopListFragment();

    @ContributesAndroidInjector
    abstract VerifyEmailFragment contributeVerifyEmailFragment();
}


@Module
abstract class ProfileEditModule {
    @ContributesAndroidInjector
    abstract ProfileEditFragment contributeProfileEditFragment();
}

@Module
abstract class TransactionModule {
    @ContributesAndroidInjector
    abstract TransactionListFragment contributeTransactionListFragment();
}

@Module
abstract class FavouriteListModule {
    @ContributesAndroidInjector
    abstract FavouriteListFragment contributeFavouriteFragment();
}

@Module
abstract class UserRegisterModule {
    @ContributesAndroidInjector
    abstract UserRegisterFragment contributeUserRegisterFragment();
}

@Module
abstract class UserForgotPasswordModule {
    @ContributesAndroidInjector
    abstract UserForgotPasswordFragment contributeUserForgotPasswordFragment();
}

@Module
abstract class UserLoginModule {
    @ContributesAndroidInjector
    abstract UserLoginFragment contributeUserLoginFragment();
}

@Module
abstract class PasswordChangeModule {
    @ContributesAndroidInjector
    abstract PasswordChangeFragment contributePasswordChangeFragment();
}

@Module
abstract class CommentDetailModule {
    @ContributesAndroidInjector
    abstract CommentDetailFragment commentDetailFragment();
}

@Module
abstract class DiscountDetailModule {
    @ContributesAndroidInjector
    abstract ProductDetailFragment discountDetailFragment();
}

@Module
abstract class NotificationModule {
    @ContributesAndroidInjector
    abstract NotificationListFragment notificationFragment();


}


@Module
abstract class NotificationDetailModule {
    @ContributesAndroidInjector
    abstract NotificationFragment notificationDetailFragment();
}

@Module
abstract class TransactionDetailModule {
    @ContributesAndroidInjector
    abstract TransactionFragment transactionDetailFragment();
}

@Module
abstract class UserHistoryModule {
    @ContributesAndroidInjector
    abstract HistoryFragment contributeHistoryFragment();
}

@Module
abstract class AppInfoModule {
    @ContributesAndroidInjector
    abstract AppInfoFragment contributeAppInfoFragment();
}

@Module
abstract class ProductCollectionModule {
    @ContributesAndroidInjector
    abstract ProductCollectionHeaderListFragment contributeProductCollectionHeaderListFragment();
}

@Module
abstract class CategoryListActivityAppInfoModule {
    @ContributesAndroidInjector
    abstract CategoryListFragment contributeCategoryFragment();

    @ContributesAndroidInjector
    abstract TrendingCategoryFragment contributeTrendingCategoryFragment();
}

@Module
abstract class RatingListActivityModule {
    @ContributesAndroidInjector
    abstract RatingListFragment contributeRatingListFragment();
}

@Module
abstract class TermsAndConditionsModule {
    @ContributesAndroidInjector
    abstract TermsAndConditionsFragment TermsAndConditionsFragment();
}

@Module
abstract class EditSettingModule {
    @ContributesAndroidInjector
    abstract SettingFragment EditSettingFragment();
}

@Module
abstract class LanguageChangeModule {
    @ContributesAndroidInjector
    abstract NotificationSettingFragment notificationSettingFragment();
}

@Module
abstract class EditProfileModule {
    @ContributesAndroidInjector
    abstract ProfileFragment ProfileFragment();
}

@Module
abstract class ProductListByCatIdModule {
    @ContributesAndroidInjector
    abstract ProductListByCatIdFragment contributeProductListByCatIdFragment();

}

@Module
abstract class FilteringModule {

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract FilterFragment contributeSpecialFilteringFragment();
}

@Module
abstract class HomeFilteringActivityModule {
    @ContributesAndroidInjector
    abstract ProductListFragment contributefeaturedProductFragment();

    @ContributesAndroidInjector
    abstract CategoryListFragment contributeCategoryFragment();

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract CollectionFragment contributeCollectionFragment();
}

@Module
abstract class CommentListActivityModule {
    @ContributesAndroidInjector
    abstract CommentListFragment contributeCommentListFragment();
}

@Module
abstract class BasketlistActivityModule {
    @ContributesAndroidInjector
    abstract BasketListFragment contributeBasketListFragment();
}

@Module
abstract class GalleryDetailActivityModule {
    @ContributesAndroidInjector
    abstract GalleryDetailFragment contributeGalleryDetailFragment();
}

@Module
abstract class GalleryActivityModule {
    @ContributesAndroidInjector
    abstract GalleryFragment contributeGalleryFragment();
}

@Module
abstract class SearchByCategoryActivityModule {

    @ContributesAndroidInjector
    abstract SearchCategoryFragment contributeSearchCategoryFragment();

    @ContributesAndroidInjector
    abstract SearchSubCategoryFragment contributeSearchSubCategoryFragment();

    @ContributesAndroidInjector
    abstract SearchCountryListFragment contributeSearchCountryListFragment();

    @ContributesAndroidInjector
    abstract SearchCityListFragment contributeSearchCityListFragment();

}

@Module
abstract class ShopListModule {

    @ContributesAndroidInjector
    abstract ShopFragment contributeAboutUsFragmentFragment();

    @ContributesAndroidInjector
    abstract BasketListFragment basketFragment();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragment();

    @ContributesAndroidInjector
    abstract SelectedShopFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract CategoryFilterFragment contributeTypeFilterFragment();

    @ContributesAndroidInjector
    abstract ProductCollectionHeaderListFragment contributeProductCollectionHeaderListFragment();

    @ContributesAndroidInjector
    abstract ShopMenuFragment contributeShopMenuFragment();


}

@Module
abstract class CollectionModule {

    @ContributesAndroidInjector
    abstract CollectionFragment contributeCollectionFragment();

}

@Module
abstract class StripeModule {

    @ContributesAndroidInjector
    abstract StripeFragment contributeStripeFragment();

}

@Module
abstract class SelectedShopListModule {

    @ContributesAndroidInjector
    abstract ShopListFragment contributeSelectedShopListFragment();

}

@Module
abstract class SelectedShopListBlogModule {

    @ContributesAndroidInjector
    abstract BlogListFragment contributeSelectedShopListBlogFragment();

}

@Module
abstract class BlogDetailModule {

    @ContributesAndroidInjector
    abstract BlogDetailFragment contributeBlogDetailFragment();
}

@Module
abstract class ShopCategoryDetailModule {

    @ContributesAndroidInjector
    abstract ShopListByTagIdFragment contributeShopCategoryDetailFragment();
}

@Module
abstract class ShopCategoryViewAllModule {

    @ContributesAndroidInjector
    abstract ShopTagListFragment contributeShopCategoryViewAllFragment();
}


@Module
abstract class forceUpdateModule {

    @ContributesAndroidInjector
    abstract ForceUpdateFragment contributeForceUpdateFragment();
}

@Module
abstract class blogListByShopIdActivityModule {

    @ContributesAndroidInjector
    abstract BlogListByShopIdFragment contributeBlogListByShopIdFragment();
}

@Module
abstract class appLoadingActivityModule {

    @ContributesAndroidInjector
    abstract AppLoadingFragment contributeAppLoadingFragment();
}
@Module
abstract class PrivacyAndPolicyActivityModule {

    @ContributesAndroidInjector
    abstract PrivacyAndPolicyFragment contributePrivacyAndPolicyFragment();

}
@Module
abstract class VerifyEmailModule {
    @ContributesAndroidInjector
    abstract VerifyEmailFragment contributeVerifyEmailFragment();
}

