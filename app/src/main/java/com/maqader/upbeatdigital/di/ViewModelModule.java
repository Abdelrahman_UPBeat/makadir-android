package com.maqader.upbeatdigital.di;

import android.content.SharedPreferences;

import com.maqader.upbeatdigital.viewmodel.aboutus.AboutUsViewModel;
import com.maqader.upbeatdigital.viewmodel.blog.BlogViewModel;
import com.maqader.upbeatdigital.viewmodel.category.CategoryViewModel;
import com.maqader.upbeatdigital.viewmodel.city.CityViewModel;
import com.maqader.upbeatdigital.viewmodel.clearalldata.ClearAllDataViewModel;
import com.maqader.upbeatdigital.viewmodel.apploading.AppLoadingViewModel;
import com.maqader.upbeatdigital.viewmodel.country.CountryViewModel;
import com.maqader.upbeatdigital.viewmodel.subcategory.SubCategoryViewModel;
import com.maqader.upbeatdigital.viewmodel.collection.ProductCollectionProductViewModel;
import com.maqader.upbeatdigital.viewmodel.collection.ProductCollectionViewModel;
import com.maqader.upbeatdigital.viewmodel.comment.CommentDetailListViewModel;
import com.maqader.upbeatdigital.viewmodel.comment.CommentListViewModel;
import com.maqader.upbeatdigital.viewmodel.common.NotificationViewModel;
import com.maqader.upbeatdigital.viewmodel.common.PSNewsViewModelFactory;
import com.maqader.upbeatdigital.viewmodel.contactus.ContactUsViewModel;
import com.maqader.upbeatdigital.viewmodel.coupondiscount.CouponDiscountViewModel;
import com.maqader.upbeatdigital.viewmodel.homelist.HomeFeaturedProductViewModel;
import com.maqader.upbeatdigital.viewmodel.homelist.HomeLatestProductViewModel;
import com.maqader.upbeatdigital.viewmodel.homelist.HomeSearchProductViewModel;
import com.maqader.upbeatdigital.viewmodel.homelist.HomeTrendingCategoryListViewModel;
import com.maqader.upbeatdigital.viewmodel.homelist.HomeTrendingProductViewModel;
import com.maqader.upbeatdigital.viewmodel.image.ImageViewModel;
import com.maqader.upbeatdigital.viewmodel.notification.NotificationsViewModel;
import com.maqader.upbeatdigital.viewmodel.paypal.PaypalViewModel;
import com.maqader.upbeatdigital.viewmodel.product.BasketViewModel;
import com.maqader.upbeatdigital.viewmodel.product.FavouriteViewModel;
import com.maqader.upbeatdigital.viewmodel.product.HistoryProductViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductAttributeDetailViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductAttributeHeaderViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductColorViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductDetailViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductFavouriteViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductListByCatIdViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductRelatedViewModel;
import com.maqader.upbeatdigital.viewmodel.product.ProductSpecsViewModel;
import com.maqader.upbeatdigital.viewmodel.product.TouchCountViewModel;
import com.maqader.upbeatdigital.viewmodel.rating.RatingViewModel;
import com.maqader.upbeatdigital.viewmodel.shippingmethod.ShippingMethodViewModel;
import com.maqader.upbeatdigital.viewmodel.shop.FeaturedShopViewModel;
import com.maqader.upbeatdigital.viewmodel.shop.LatestShopViewModel;
import com.maqader.upbeatdigital.viewmodel.shop.PopularShopViewModel;
import com.maqader.upbeatdigital.viewmodel.shop.ShopViewModel;
import com.maqader.upbeatdigital.viewmodel.shoptag.ShopTagViewModel;
import com.maqader.upbeatdigital.viewmodel.transaction.TransactionListViewModel;
import com.maqader.upbeatdigital.viewmodel.transaction.TransactionOrderViewModel;
import com.maqader.upbeatdigital.viewmodel.user.UserViewModel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Component;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Panacea-Soft on 11/16/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Module
abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(PSNewsViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel.class)
    abstract ViewModel bindUserViewModel(UserViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AboutUsViewModel.class)
    abstract ViewModel bindAboutUsViewModel(AboutUsViewModel aboutUsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ImageViewModel.class)
    abstract ViewModel bindImageViewModel(ImageViewModel imageViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RatingViewModel.class)
    abstract ViewModel bindRatingViewModel(RatingViewModel ratingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel.class)
    abstract ViewModel bindNotificationViewModel(NotificationViewModel notificationViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ContactUsViewModel.class)
    abstract ViewModel bindContactUsViewModel(ContactUsViewModel contactUsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CountryViewModel.class)
    abstract ViewModel bindCountryViewModel(CountryViewModel countryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CityViewModel.class)
    abstract ViewModel bindCityViewModel(CityViewModel cityViewModel);

  /*  @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel.class)
    abstract ViewModel bindProductViewModel(ProductViewModel productViewModel);*/

    @Binds
    @IntoMap
    @ViewModelKey(CommentListViewModel.class)
    abstract ViewModel bindCommentViewModel(CommentListViewModel commentListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CommentDetailListViewModel.class)
    abstract ViewModel bindCommentDetailViewModel(CommentDetailListViewModel commentDetailListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel.class)
    abstract ViewModel bindProductDetailViewModel(ProductDetailViewModel productDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FavouriteViewModel.class)
    abstract ViewModel bindFavouriteViewModel(FavouriteViewModel favouriteViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TouchCountViewModel.class)
    abstract ViewModel bindTouchCountViewModel(TouchCountViewModel touchCountViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductColorViewModel.class)
    abstract ViewModel bindProductColorViewModel(ProductColorViewModel productColorViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductSpecsViewModel.class)
    abstract ViewModel bindProductSpecsViewModel(ProductSpecsViewModel productSpecsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BasketViewModel.class)
    abstract ViewModel bindBasketViewModel(BasketViewModel basketViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HistoryProductViewModel.class)
    abstract ViewModel bindHistoryProductViewModel(HistoryProductViewModel historyProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductAttributeHeaderViewModel.class)
    abstract ViewModel bindProductAttributeHeaderViewModel(ProductAttributeHeaderViewModel productAttributeHeaderViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductAttributeDetailViewModel.class)
    abstract ViewModel bindProductAttributeDetailViewModel(ProductAttributeDetailViewModel productAttributeDetailViewModel);
/*

    @Binds
    @IntoMap
    @ViewModelKey(DiscountProductViewModel.class)
    abstract ViewModel bindDiscountProductViewModel(DiscountProductViewModel discountProductViewModel);
*/

    @Binds
    @IntoMap
    @ViewModelKey(ProductRelatedViewModel.class)
    abstract ViewModel bindRelatedProductViewModel(ProductRelatedViewModel productRelatedViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductFavouriteViewModel.class)
    abstract ViewModel bindProductFavouriteViewModel(ProductFavouriteViewModel productFavouriteViewModel);

//    @Binds
//    @IntoMap
//    @ViewModelKey(ProductLikedViewModel.class)
//    abstract ViewModel bindProductLikedViewModel(ProductLikedViewModel productLikedViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel.class)
    abstract ViewModel bindCategoryViewModel(CategoryViewModel categoryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SubCategoryViewModel.class)
    abstract ViewModel bindSubCategoryViewModel(SubCategoryViewModel subCategoryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductListByCatIdViewModel.class)
    abstract ViewModel bindProductListByCatIdViewModel(ProductListByCatIdViewModel productListByCatIdViewModel);

//    @Binds
//    @IntoMap
//    @ViewModelKey(TrendingProductsViewModel.class)
//    abstract ViewModel bindTrendingProductsViewModel(TrendingProductsViewModel trendingProductsViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeLatestProductViewModel.class)
    abstract ViewModel bindHomeLatestProductViewModel(HomeLatestProductViewModel homeLatestProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeSearchProductViewModel.class)
    abstract ViewModel bindHomeFeaturedProductViewModel(HomeSearchProductViewModel homeSearchProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeTrendingProductViewModel.class)
    abstract ViewModel bindHomeTrendingProductViewModel(HomeTrendingProductViewModel homeTrendingProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeFeaturedProductViewModel.class)
    abstract ViewModel bindHomeCategory1ProductViewModel(HomeFeaturedProductViewModel homeFeaturedProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductCollectionViewModel.class)
    abstract ViewModel bindProductCollectionViewModel(ProductCollectionViewModel productCollectionViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotificationsViewModel.class)
    abstract ViewModel bindNotificationListViewModel(NotificationsViewModel notificationListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TransactionListViewModel.class)
    abstract ViewModel bindTransactionListViewModel(TransactionListViewModel transactionListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TransactionOrderViewModel.class)
    abstract ViewModel bindTransactionOrderViewModel(TransactionOrderViewModel transactionOrderViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeTrendingCategoryListViewModel.class)
    abstract ViewModel bindHomeTrendingCategoryListViewModel(HomeTrendingCategoryListViewModel transactionListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProductCollectionProductViewModel.class)
    abstract ViewModel bindProductCollectionProductViewModel(ProductCollectionProductViewModel productCollectionProductViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FeaturedShopViewModel.class)
    abstract ViewModel bindFeaturedShopViewModel(FeaturedShopViewModel shopViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShopViewModel.class)
    abstract ViewModel bindShopViewModel(ShopViewModel shopViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LatestShopViewModel.class)
    abstract ViewModel bindRecentShopViewModel(LatestShopViewModel latestShopViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PopularShopViewModel.class)
    abstract ViewModel bindNewShopViewModel(PopularShopViewModel popularShopViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShopTagViewModel.class)
    abstract ViewModel bindShopCategoryViewModel(ShopTagViewModel shopTagViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BlogViewModel.class)
    abstract ViewModel bindNewsFeedViewModel(BlogViewModel blogViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShippingMethodViewModel.class)
    abstract ViewModel bindShippingMethodViewModel(ShippingMethodViewModel shippingMethodViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PaypalViewModel.class)
    abstract ViewModel bindPaypalViewModel(PaypalViewModel paypalViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CouponDiscountViewModel.class)
    abstract ViewModel bindCouponDiscountViewModel(CouponDiscountViewModel couponDiscountViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AppLoadingViewModel.class)
    abstract ViewModel bindPSAppInfoViewModel(AppLoadingViewModel psAppInfoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ClearAllDataViewModel.class)
    abstract ViewModel bindClearAllDataViewModel(ClearAllDataViewModel clearAllDataViewModel);



}


