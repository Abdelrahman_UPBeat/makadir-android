package com.maqader.upbeatdigital.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.maqader.upbeatdigital.db.common.Converters;
import com.maqader.upbeatdigital.viewobject.AboutUs;
import com.maqader.upbeatdigital.viewobject.Basket;
import com.maqader.upbeatdigital.viewobject.Blog;
import com.maqader.upbeatdigital.viewobject.Category;
import com.maqader.upbeatdigital.viewobject.CategoryMap;
import com.maqader.upbeatdigital.viewobject.City;
import com.maqader.upbeatdigital.viewobject.Comment;
import com.maqader.upbeatdigital.viewobject.CommentDetail;
import com.maqader.upbeatdigital.viewobject.Country;
import com.maqader.upbeatdigital.viewobject.DeletedObject;
import com.maqader.upbeatdigital.viewobject.DiscountProduct;
import com.maqader.upbeatdigital.viewobject.FavouriteProduct;
import com.maqader.upbeatdigital.viewobject.FeaturedProduct;
import com.maqader.upbeatdigital.viewobject.HistoryProduct;
import com.maqader.upbeatdigital.viewobject.Image;
import com.maqader.upbeatdigital.viewobject.LatestProduct;
import com.maqader.upbeatdigital.viewobject.LikedProduct;
import com.maqader.upbeatdigital.viewobject.Noti;
import com.maqader.upbeatdigital.viewobject.PSAppInfo;
import com.maqader.upbeatdigital.viewobject.PSAppVersion;
import com.maqader.upbeatdigital.viewobject.Product;
import com.maqader.upbeatdigital.viewobject.ProductAttributeDetail;
import com.maqader.upbeatdigital.viewobject.ProductAttributeHeader;
import com.maqader.upbeatdigital.viewobject.ProductCollection;
import com.maqader.upbeatdigital.viewobject.ProductCollectionHeader;
import com.maqader.upbeatdigital.viewobject.ProductColor;
import com.maqader.upbeatdigital.viewobject.ProductListByCatId;
import com.maqader.upbeatdigital.viewobject.ProductMap;
import com.maqader.upbeatdigital.viewobject.ProductSpecs;
import com.maqader.upbeatdigital.viewobject.Rating;
import com.maqader.upbeatdigital.viewobject.RelatedProduct;
import com.maqader.upbeatdigital.viewobject.ShippingMethod;
import com.maqader.upbeatdigital.viewobject.Shop;
import com.maqader.upbeatdigital.viewobject.ShopByTagId;
import com.maqader.upbeatdigital.viewobject.ShopMap;
import com.maqader.upbeatdigital.viewobject.ShopTag;
import com.maqader.upbeatdigital.viewobject.SubCategory;
import com.maqader.upbeatdigital.viewobject.TransactionDetail;
import com.maqader.upbeatdigital.viewobject.TransactionObject;
import com.maqader.upbeatdigital.viewobject.User;
import com.maqader.upbeatdigital.viewobject.UserLogin;


/**
 * Created by Panacea-Soft on 11/20/17.
 * Contact Email : teamps.is.cool@gmail.com
 */

@Database(entities = {
        Image.class,
        Category.class,
        User.class,
        UserLogin.class,
        AboutUs.class,
        Product.class,
        LatestProduct.class,
        DiscountProduct.class,
        FeaturedProduct.class,
        SubCategory.class,
        ProductListByCatId.class,
        Comment.class,
        CommentDetail.class,
        ProductColor.class,
        ProductSpecs.class,
        RelatedProduct.class,
        FavouriteProduct.class,
        LikedProduct.class,
        ProductAttributeHeader.class,
        ProductAttributeDetail.class,
        Noti.class,
        TransactionObject.class,
        ProductCollectionHeader.class,
        ProductCollection.class,
        TransactionDetail.class,
        Basket.class,
        HistoryProduct.class,
        Shop.class,
        ShopTag.class,
        Blog.class,
        Rating.class,
        ShippingMethod.class,
        ShopByTagId.class,
        ProductMap.class,
        ShopMap.class,
        CategoryMap.class,
        PSAppInfo.class,
        PSAppVersion.class,
        DeletedObject.class,
        Country.class,
        City.class

}, version = 6, exportSchema = false)
//V2.1 = DBV 6
//V2.0 = DBV 5
//V1.6 = DBV 4
//V1.5 = DBV 4
//V1.4 = DBV 3
//V1.3 = DBV 2


@TypeConverters({Converters.class})

public abstract class PSCoreDb extends RoomDatabase {

    abstract public UserDao userDao();

    abstract public ProductColorDao productColorDao();

    abstract public ProductSpecsDao productSpecsDao();

    abstract public ProductAttributeHeaderDao productAttributeHeaderDao();

    abstract public ProductAttributeDetailDao productAttributeDetailDao();

    abstract public BasketDao basketDao();

    abstract public HistoryDao historyDao();

    abstract public AboutUsDao aboutUsDao();

    abstract public ImageDao imageDao();

    abstract public RatingDao ratingDao();

    abstract public CommentDao commentDao();

    abstract public CommentDetailDao commentDetailDao();

    abstract public ProductDao productDao();

    abstract public CategoryDao categoryDao();

    abstract public CountryDao countryDao();

    abstract public CityDao cityDao();

    abstract public SubCategoryDao subCategoryDao();

    abstract public NotificationDao notificationDao();

    abstract public ProductCollectionDao productCollectionDao();

    abstract public TransactionDao transactionDao();

    abstract public TransactionOrderDao transactionOrderDao();

    abstract public ShopDao shopDao();

    abstract public ShopTagDao shopCategoryDao();

    abstract public BlogDao blogDao();

    abstract public ShippingMethodDao shippingMethodDao();

    abstract public ShopListByTagIdDao shopListByTagIdDao();

    abstract public ProductMapDao productMapDao();

    abstract public ShopMapDao shopMapDao();

    abstract public CategoryMapDao categoryMapDao();

    abstract public PSAppInfoDao psAppInfoDao();

    abstract public PSAppVersionDao psAppVersionDao();

    abstract public DeletedObjectDao deletedObjectDao();


//    /**
//     * Migrate from:
//     * version 1 - using Room
//     * to
//     * version 2 - using Room where the {@link } has an extra field: addedDateStr
//     */
//    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE news "
//                    + " ADD COLUMN addedDateStr INTEGER NOT NULL DEFAULT 0");
//        }
//    };

    /* More migration write here */
}