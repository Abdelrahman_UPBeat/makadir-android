package com.maqader.upbeatdigital.ui.shop.list;

import android.os.Bundle;

import com.maqader.upbeatdigital.R;
import com.maqader.upbeatdigital.databinding.ActivityShopListBinding;
import com.maqader.upbeatdigital.ui.common.PSAppCompactActivity;
import com.maqader.upbeatdigital.utils.Constants;

import androidx.databinding.DataBindingUtil;

public class ShopListActivity extends PSAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityShopListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_list);

        initUI(binding);

    }

    private void initUI(ActivityShopListBinding binding) {

        String title = getIntent().getStringExtra(Constants.SHOP_TITLE);

        if (title != null) {
            initToolbar(binding.toolbar, title);
        } else {
            initToolbar(binding.toolbar, getResources().getString(R.string.shop__list));
        }

        // setup Fragment
        setupFragment(new ShopListFragment());

    }
}
