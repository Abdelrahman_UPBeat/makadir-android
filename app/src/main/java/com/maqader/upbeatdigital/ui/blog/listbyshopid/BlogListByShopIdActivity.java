package com.maqader.upbeatdigital.ui.blog.listbyshopid;


import android.os.Bundle;

import com.maqader.upbeatdigital.R;
import com.maqader.upbeatdigital.databinding.ActivityBlogListByShopIdBinding;
import com.maqader.upbeatdigital.ui.common.PSAppCompactActivity;

import androidx.databinding.DataBindingUtil;

public class BlogListByShopIdActivity extends PSAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityBlogListByShopIdBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_blog_list_by_shop_id);

        initUI(binding);

    }

    private void initUI(ActivityBlogListByShopIdBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.blog_list__title));

        // setup Fragment

        setupFragment(new BlogListByShopIdFragment());

    }
}
