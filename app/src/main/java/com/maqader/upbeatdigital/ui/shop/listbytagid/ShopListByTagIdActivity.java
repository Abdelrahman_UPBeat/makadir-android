package com.maqader.upbeatdigital.ui.shop.listbytagid;

import android.os.Bundle;

import com.maqader.upbeatdigital.R;
import com.maqader.upbeatdigital.databinding.ActivityShopListByTagIdBinding;
import com.maqader.upbeatdigital.ui.common.PSAppCompactActivity;
import com.maqader.upbeatdigital.utils.Constants;

import androidx.databinding.DataBindingUtil;

public class ShopListByTagIdActivity extends PSAppCompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityShopListByTagIdBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_shop_list_by_tag_id);

        initUI(binding);

    }

    private void initUI(ActivityShopListByTagIdBinding binding) {

        // Toolbar

        String title = getIntent().getStringExtra(Constants.SHOP_TITLE);

        if(title != null)
        {
            initToolbar(binding.toolbar, title);
        }else {
            initToolbar(binding.toolbar, getResources().getString(R.string.shop__list));
        }

        // setup Fragment
        setupFragment(new ShopListByTagIdFragment());

    }
}
