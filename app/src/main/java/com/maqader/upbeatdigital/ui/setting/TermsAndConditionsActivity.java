package com.maqader.upbeatdigital.ui.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.maqader.upbeatdigital.Config;
import com.maqader.upbeatdigital.R;
import com.maqader.upbeatdigital.databinding.ActivityTermsAndConditionBinding;
import com.maqader.upbeatdigital.ui.common.PSAppCompactActivity;
import com.maqader.upbeatdigital.ui.terms.TermsAndConditionsFragment;
import com.maqader.upbeatdigital.utils.Constants;
import com.maqader.upbeatdigital.utils.MyContextWrapper;

import androidx.databinding.DataBindingUtil;

public class TermsAndConditionsActivity extends PSAppCompactActivity {

    //region Override Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityTermsAndConditionBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_and_conditions);

        // Init all UI
        initUI(binding);

    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString(Constants.LANGUAGE_CODE, Config.DEFAULT_LANGUAGE);
        String CURRENT_LANG_COUNTRY_CODE = preferences.getString(Constants.LANGUAGE_COUNTRY_CODE, Config.DEFAULT_LANGUAGE_COUNTRY_CODE);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT, CURRENT_LANG_COUNTRY_CODE, true));

    }
    //endregion


    //region Private Methods

    private void initUI(ActivityTermsAndConditionBinding binding) {

        // Toolbar
        initToolbar(binding.toolbar, getResources().getString(R.string.terms_and_conditions__title));

        // setup Fragment
        setupFragment(new TermsAndConditionsFragment());

    }

    //endregion

}
